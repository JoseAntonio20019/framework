<?php 

    class Controller{


        public function model($modelo){

            require_once '../app/models/'. $modelo. '.php';
            return new $modelo;


        }


        public function view($vista,$datos=[]){

            
            if(file_exists('../app/views/'.$vista.'.php')){

                    

                require_once '../app/views/'. $vista .'.php';
                
            }else{

                die("\n No existe esta vista");
            }


        }   



    }


?>